<!DOCTYPE HTML> 
<html lang='es'>    
<head>      
    <title>Aplicacion CashConverters</title>   
<meta charset='UTF-8' /> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 

 <!-- Css Styles -->
 <link rel="stylesheet" href="css/style_bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">


</head>
 <body>

  <?php

    session_start();

    if(!isset($_SESSION["conectado"])){

      header("Location: php/login.php");

    }else{

  ?>
 
      <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="dashboard.php">Tienda</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="imagenes/user1.png" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name"><?php echo $_SESSION["conectado"];?></h5>
                                    <span class="badge-dot badge-success mr-1"></span><span class="status"></span><span class="ml-2">Conectado</span>
                                </div>
                                <a class="dropdown-item" href="portfolio/dashboard.php"><i class="fa fa-shopping-cart"></i> Panel de control</a>
                                <a class="dropdown-item logout" href="#"><i class="fa fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col"><br><br>
          <h1 class="text-center">Aplicación CashConverters</h1><hr>
        </div>
      </div><br>

        <div id="confirmacion" class="msg" style="display: none;">
          <label class="confirmacion" id="confirm"></label> 
        </div> 
        <div id="confirmacionAlmacen" class="msg" style="display: none;">
          <label class="confirmacionAlmacen" id="confirmAlm"></label> 
        </div> 

        <div class="row">
          <div class="col-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 menuLateral" id="collapsibleNavbar">
 <!--
           <section>
              <a class="button-19" onclick="comprar()">
                <i class="fa fa-shopping-cart"></i> <span>Realizar una compra</span>
              </a><br><br>             
              <a class="button-19" onclick="vender()">
                <i class="fa fa-shopping-basket"></i> <span>Realizar una venta</span>
              </a><br> <br>         
              <a class="button-19" onclick="almacen()">
                <i class="fa fa-server"></i> <span>Almacen</span>
              </a><br> <br>            
              <a class="button-19" onclick="cierre()">
                <i class="fa fa-balance-scale"></i> <span>Cierre diario</span>
              </a><br> <br>           
              <a class="button-19" onclick="restaurar()">
                <i class="fa fa-undo"></i> <span>Restaurar datos</span>
              </a><br> <br>           
              <a class="button-19" onclick="cargaAlmacen()">
                <i class="fa fa-download"></i> <span>Cargar almacen</span>
              </a><br>
            </section> 
 -->           
            
            <div class="nav-left-sidebar">
              <div class="menu-list">
                  <nav class="navbar navbar-expand-lg navbar-light">
                      <a class="d-xl-none d-lg-none" href="#">Menu</a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                      </button>
                      <div class="collapse navbar-collapse" id="navbarNav">
                          <ul class="navbar-nav flex-column">
                              <li class="nav-item ">
                                  <a class="nav-link active button-19" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-1" onclick="comprar()"><i class="fa fa-shopping-cart"></i> Realizar una compra</a>
                              </li><br>
                              <li class="nav-item ">
                                  <a class="nav-link active button-19" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2" onclick="vender()"><i class="fa fa-shopping-basket"></i> Realizar una venta</a>
                              </li><br> 
                              <li class="nav-item ">
                                  <a class="nav-link active button-17" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3" onclick="almacen()"><i class="fa fa-server"></i> Almacén</a>
                              </li><br>
                              <li class="nav-item ">
                                  <a class="nav-link active button-16" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4" onclick="cierre()"><i class="fa fa-balance-scale"></i> Cierre diario</a>
                              </li><br>
                              <li class="nav-item ">
                                  <a class="nav-link active button-18" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5" onclick="restaurar()"><i class="fa fa-undo"></i> Restaurar datos</a>
                              </li><br>
                              <li class="nav-item ">
                                  <a class="nav-link active button-18" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6" onclick="cargaAlmacen()"><i class="fa fa-download"></i> Cargar almacén</a>
                              </li>                          
                          </ul>
                      </div>
                  </nav>
              </div>
            </div>
          </div>
          <div class="col-9 col-lg-9 col-md-12 col-sm-12 col-xs-12" id="form1">

<!-- Formulario de compra -->
          <div class="card">
            <div class="mt-2" id="formularioCompra" style="display: none;">
              <h2>Formulario de compra de artículos</h2><br>
              <form name="formCompra" id="formCompra">
                <div class="form-group">
                  <label for="tipoArt">Selecciona el tipo de artículo</label>
                  <select class="form-control" name="tipoArt" id="tipoArt">
                    <option value="Telefonia">Telefonía</option>
                    <option value="Informatica">Informática</option>
                    <option value="Videojuegos">Videojuegos</option>
                    <option value="Consolas">Consolas</option>
                    <option value="Electrodomesticos">Electrodomésticos</option>
                    <option value="TV/Video">TV - Vídeo</option>
                    <option value="Sonido">Sonido</option>
                  </select>
                </div><br>
                <div class="form-group">
                  <label for="nombreArt">Nombre del artículo:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el nombre del artículo" name="nombreArt" id="nombreArt">
                  <label id="articuloError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="estado">¿Cuál es el estado del artículo?</label>
                  <select class="form-control" name="estado" id="estado">
                    <option value="Bueno">Bueno</option>
                    <option value="Regular">Regular</option>
                    <option value="Malo">Malo</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="precio">Precio de compra:</label>
                  <input type="number" class="form-control" placeholder="Introduzca una cantidad máximo 1000€" name="precio" id="precio">
                  <label id="precioError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="nombre">Nombre del cliente:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el nombre del cliente" name="nombre" id="nombre">
                  <label id="nombreError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="dni">DNI del cliente:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el DNI del cliente" name="dni" id="dni">
                  <label id="dniError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="telefono">Teléfono:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el telefono del cliente" name="telefono" id="telefono">
                  <label id="tlfError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="mail">Mail:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el mail del cliente" name="mail" id="mail">
                  <label id="mailError" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="direccion">Dirección:</label>
                  <input type="text" class="form-control" placeholder="Introduzca la direccion del cliente" name="direccion" id="direccion">
                  <label id="dirError" class="errores"></label>
                </div>
                <br>
                <button class="boton1" type="button" onclick="crearCompra()" style="display: block;"> 
                  <div class="default-btn1">
                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="#ffd300" height="20" width="20" viewBox="0 0 24 24"><circle r="1" cy="21" cx="9"></circle><circle r="1" cy="21" cx="20"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                    <span id="creaCompra">Tramitar compra</span>
                  </div>
                </button>
              </form>
            </div>
    </div>
<!-- Formulario de venta -->

            <div class="mt-2" id="formularioVenta" style="display: none;">
              <h2>Formulario de venta de artículos</h2><br>
              <form name="formVenta" id="formVenta">
                <div class="form-group">
                  <label for="artEnVenta">Seleccionar el artículo a vender</label>
                  <select class="form-control" name="artEnVenta" id="artEnVenta" onchange="cargarEstados()">
                  </select>
                </div><br>
                 <div class="form-group">
                  <label for="artEnVenta">Estado</label>
                  <select class="form-control" name="estadoVenta" id="estadoVenta">
                  </select>
                </div>
                <div class="form-group">
                  <label for="nombreComprador">Nombre del cliente:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el nombre del cliente" name="nombreComprador" id="nombreComprador">
                  <label id="nombreE" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="dniComprador">DNI del cliente:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el DNI del cliente" name="dniComprador" id="dniComprador">
                  <label id="dniE" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="telefonoComprador">Teléfono:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el telefono del cliente" name="telefonoComprador" id="telefonoComprador">
                  <label id="tlfE" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="mailComprador">Mail:</label>
                  <input type="text" class="form-control" placeholder="Introduzca el mail del cliente" name="mailComprador" id="mailComprador">
                  <label id="mailE" class="errores"></label>
                </div>
                <div class="form-group">
                  <label for="direccionComprador">Dirección:</label>
                  <input type="text" class="form-control" placeholder="Introduzca la direccion del cliente" name="direccionComprador" id="direccionComprador">
                  <label id="dirE" class="errores"></label>
                </div>
                <br>
                <button class="boton1" type="button" onclick="crearVenta()" style="display: block;"> 
                  <div class="default-btn1">
                    <svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="#ffd300" height="20" width="20" viewBox="0 0 24 24"><circle r="1" cy="21" cx="9"></circle><circle r="1" cy="21" cx="20"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                    <span id="creaVenta">Tramitar venta</span>
                  </div>
                </button>
              </form><br>
            </div>
        

         <!-- Tablas del cierre del dia con todas las compras/ventas -->
            <div class="mt-3" id="tablasCierre" style="display: none;">
              <h2>Recaudacion diaria</h2><br>
                <h4 id="resultadoFinal"></h4><br>
                <button type="button" id="finDia" class="boton3" onclick="finCaja()"><div class="default-btn3"><svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="#ffd300" height="20" width="20" viewBox="0 0 24 24"><circle r="1" cy="21" cx="9"></circle><circle r="1" cy="21" cx="20"></circle><path fill="none" d="M18.807,0.337h-3.616v1.808c0,0.475-0.384,0.859-0.859,0.859c-0.474,0-0.859-0.384-0.859-0.859V0.337H6.731
							v1.808c0,0.475-0.384,0.859-0.859,0.859c-0.474,0-0.859-0.384-0.859-0.859V0.337h-3.82c-0.474,0-0.859,0.384-0.859,0.859v17.61
							c0,0.477,0.384,0.859,0.859,0.859h17.613c0.474,0,0.859-0.382,0.859-0.859V1.195C19.665,0.721,19.281,0.337,18.807,0.337z
							 M17.948,17.946H2.052V4.528h15.896V17.946z"></path></svg><span id="finDia">Finalizar día</span></div></button>
                <br><br>
                <div class="card">
                  <h5 class="card-header">Tabla de compras</h5>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table" id="tablaCompras">
                        </table>
                      </div>
                    </div>
                </div><br><br>
                <div class="card">
                  <h5 class="card-header">Tabla de ventas</h5>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table" id="tablaVentas">
                        </table>
                      </div>
                    </div>
                </div><br><br>
            </div>

          <!-- Tablas del almacen de los productos que aun no han sido vendidos -->
          
            <div class="mt-3" id="tablaAlmacen" style="display: none;">
              <h2>Articulos en el almacén</h2>
                <div class="card">
                  <h5 class="card-header">Artículos</h5>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table" id="ArticulosAlmacenados">
                        </table>
                      </div>
                    </div>
                </div>
            </div>
          </div>
          
            <!-- Datos de la compra -->
            <div class="vacia" id="datosCompra" style="display: none;">
              
            </div>
          

            <!-- Datos de la venta -->
            <div class="vacia" id="datosVenta" style="display: none;">
              
            </div>

          </div>
        <div>      
    </div>

  <?php
    }
  ?>
</body>

    <!-- Scripts javascript-->
    <script src="js/main.js"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.7/js/dataTables.autoFill.min.js"></script>
    <script src="https://cdn.datatables.net/autofill/2.3.7/js/autoFill.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.1/js/buttons.bootstrap4.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.5/js/dataTables.colReorder.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/4.0.1/js/dataTables.fixedColumns.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.2.0/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/keytable/2.6.4/js/dataTables.keyTable.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.1.4/js/dataTables.rowGroup.min.js"></script>

    <script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/scroller/2.0.5/js/dataTables.scroller.min.js"></script>
    <script src="https://cdn.datatables.net/searchbuilder/1.3.0/js/dataTables.searchBuilder.min.js"></script>
    <script src="https://cdn.datatables.net/searchpanes/1.4.0/js/dataTables.searchPanes.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>

    <script src="https://cdn.datatables.net/staterestore/1.0.1/js/dataTables.stateRestore.min.js"></script>

</html> 
