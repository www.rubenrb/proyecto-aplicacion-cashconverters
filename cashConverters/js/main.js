var compras = new Array();
var ventas = new Array();
var clientesTienda = new Array();
var respaldoCompras = new Array();
var nombreArticulos = new Array();
var articulosUnicos = new Array();
var balance = new Array();

/* Objetos */

function compra(tipoArticulo, articulo, estado, precio, nombre, dni, precioTotal) {

    this.tipoArticulo = tipoArticulo;
    this.articulo = articulo;
    this.estado = estado;
    this.precio = parseFloat(precio);
    this.iva = precio * 0.21;
    this.nombre = nombre;
    this.dni = dni;
    this.precioTotal = precioTotal;
}

function venta(tipoArt, nombreArt, estado, precioBase, incremento, ivaVenta, precioVenta, nombre, dni) {

    this.tipoArt = tipoArt;
    this.nombreArt = nombreArt
    this.estado = estado;
    this.precioBase = precioBase;
    this.incremento = incremento
    this.ivaVenta = ivaVenta;
    this.precioVenta = precioVenta;
    this.nombre = nombre;
    this.dni = dni;

}

function cliente(nombre, dni, telefono, mail, direccion) {

    this.nombre = nombre;
    this.dni = dni
    this.telefono = telefono;
    this.mail = mail;
    this.direccion = direccion

}

/* Funciones del apartado de compra */

function comprar() {

    document.getElementById("formularioCompra").style.display = "block";
    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("tablaAlmacen").style.display = "none";
    document.getElementById("tablasCierre").style.display = "none";
    document.getElementById("confirmacion").style.display = "none";
    document.getElementById("confirmacionAlmacen").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";

    document.getElementById("formVenta").reset();

    document.getElementById("dniError").innerHTML = "";
    document.getElementById("nombreError").innerHTML = "";
    document.getElementById("precioError").innerHTML = "";
    document.getElementById("articuloError").innerHTML = "";



    dato1 = document.getElementById("creaCompra").innerHTML;

    if (dato1 != "Tramitar compra") {

        document.getElementById("creaCompra").innerHTML = "Finalizar compra";
        document.getElementById("creaCompra").onclick = function() { crearCompra() };
        document.getElementById("form1").className = "col-9 col-md-9 col-sm-12 col-xs-12";
    }
}

function crearCompra() {

    var comprobacion = true;

    var tipoArt = document.getElementById("tipoArt").value;
    var nombreArt = document.getElementById("nombreArt").value;
    var estado = document.getElementById("estado").value;
    var precio = document.getElementById("precio").value;
    precio = parseFloat(precio);
    var nombre = document.getElementById("nombre").value;
    var dni = document.getElementById("dni").value;
    var tlf = document.getElementById("telefono").value;
    var mail = document.getElementById("mail").value;
    var dir = document.getElementById("direccion").value;

    document.getElementById("dniError").innerHTML = "";
    document.getElementById("nombreError").innerHTML = "";
    document.getElementById("precioError").innerHTML = "";
    document.getElementById("articuloError").innerHTML = "";
    document.getElementById("tlfError").innerHTML = "";
    document.getElementById("mailError").innerHTML = "";
    document.getElementById("dirError").innerHTML = "";

    if (precio != "") {

        if (precio >= 1001 || precio <= 0) {

            document.getElementById("precioError").innerHTML = " El precio debe ser entre 0 y 1000 €";
            comprobacion = false;
        }

    }
    if (isNaN(precio)) {
        document.getElementById("precioError").innerHTML = " Establezca un precio de compra";
        comprobacion = false;
    }

    if (nombreArt == "") {

        document.getElementById("articuloError").innerHTML = " Introduce el nombre del articulo";
        comprobacion = false;

    }

    if (dir == "") {

        document.getElementById("dirError").innerHTML = " Introduce el la direccion del cliente";
        comprobacion = false;

    }

    var compruebaNombre = comprobarNombre(nombre);

    if (compruebaNombre == false) {

        document.getElementById("nombreError").innerHTML = " Nombre de cliente no valido";

    }

    var compruebaDni = comprobarDNI(dni);

    if (compruebaDni == false) {

        document.getElementById("dniError").innerHTML = " DNI no valido";

    }

    var compruebatlf = comprobarTelefono(tlf);

    if (compruebatlf == false) {

        document.getElementById("tlfError").innerHTML = " Telefono no valido";

    }

    var compruebamail = comprobarMail(mail);

    if (compruebamail == false) {

        document.getElementById("mailError").innerHTML = " Email no valido";

    }

    if (comprobacion == true && compruebaNombre == true && compruebaDni == true && compruebatlf == true && compruebamail == true) {

        var cadena = "";

        var precTotal = precio + (precio * 0.21);

        document.getElementById("creaCompra").innerHTML = "Completar compra";
        document.getElementById("creaCompra").onclick = function() { finCompra(tipoArt, nombreArt, estado, precio, nombre, dni, precTotal, tlf, mail, dir) };
        document.getElementById("form1").className = "col-6 col-lg-5 col-md-12 col-sm-12 col-xs-12";

        document.getElementById("datosCompra").style.display = "block";
        document.getElementById("datosCompra").className = "col-2 col-lg-3 col-md-6 col-sm-8 col-xs-8";
        document.getElementById("formularioCompra").style.marginRight = "20%";

        cadena += '<h4 align="center"><img src="imagenes/carrito.png" alt="carrito" width="40" height="50"><b> Datos de la compra</b><h4>';
        cadena += "<hr  size=3><br>";
        cadena += '<div align="center"><p><strong>Categoría: </strong> ' + tipoArt + '</p>';
        cadena += "<p><strong>Articulo: </strong> " + nombreArt + "</p>";
        cadena += "<p><strong>Precio base: </strong> " + precio + "€</p>";
        cadena += "<p><strong>Iva: </strong>  21% </p>";
        cadena += "<p><strong>Precio de compra: </strong> " + precTotal + "€</p></div>";
        /*cadena += '<button type="button" id="cancelarCompra" class="btn btn-danger" onclick="cancelarC()">Cancelar</button>';*/
        cadena += '<br><br><button type="button" id="cancelarCompra" class="boton2" onclick="cancelarC()"><div class="default-btn2"><svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="#ffd300" height="20" width="20" viewBox="0 0 24 24"><circle r="1" cy="21" cx="9"></circle><circle r="1" cy="21" cx="20"></circle><path fill="none" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path></svg><span id="creaCompra">Cancelar</span></div></button>';

        document.getElementById("datosCompra").innerHTML = cadena;

    }

}

function finCompra(tipoArt, nombreArt, estado, precio, nombre, dni, precTotal, tlf, mail, dir) {

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";
    document.getElementById("confirmacion").style.display = "block";
    document.getElementById("confirm").innerHTML = "<h3>Compra realizada con exito</h3>";

    var nuevaCompra = new compra(tipoArt, nombreArt, estado, precio, nombre, dni, precTotal);
    compras.push(nuevaCompra);

    var nuevoRespaldo = new compra(tipoArt, nombreArt, estado, precio, nombre, dni, precTotal);
    respaldoCompras.push(nuevoRespaldo);

    var nuevoCliente = new cliente(nombre, dni, tlf, mail, dir);
    clientesTienda.push(nuevoCliente);

    document.getElementById("formCompra").reset();

    localStorage.setItem('compras', JSON.stringify(compras));
    localStorage.setItem('respaldoCompras', JSON.stringify(respaldoCompras));
    localStorage.setItem('cliente', JSON.stringify(clientesTienda));

}


/* Funciones del apartado de ventas */

function vender() {

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("formularioVenta").style.display = "block";
    document.getElementById("tablaAlmacen").style.display = "none";
    document.getElementById("tablasCierre").style.display = "none";
    document.getElementById("confirmacion").style.display = "none";
    document.getElementById("confirmacionAlmacen").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";

    document.getElementById("formCompra").reset();

    document.getElementById("dniE").innerHTML = "";
    document.getElementById("nombreE").innerHTML = "";

    nombreArticulos = [];
    articulosUnicos = [];

    for (var i = 0; i < compras.length; i++) {

        nombreArticulos.push(compras[i].articulo);

    }

    for (var i = 0; i < nombreArticulos.length; i++) {

        var elemento = nombreArticulos[i];

        if (!articulosUnicos.includes(nombreArticulos[i])) {
            articulosUnicos.push(elemento);
        }
    }

    dato2 = document.getElementById("creaVenta").innerHTML;

    if (dato2 != "Tramitar venta") {

        document.getElementById("creaVenta").innerHTML = "Finalizar venta";
        document.getElementById("creaVenta").onclick = function() { crearVenta() };
        document.getElementById("form1").className = "col-9 col-md-9 col-sm-12 col-xs-12";
    }

    var opSelect = document.getElementById("artEnVenta");

    for (var i = opSelect.options.length - 1; i >= 0; i--) {
        opSelect.remove(i);
    }

    // $("#artEnVenta").empty();  misma funcion que el for anterior pero con jquery

    for (var i = 0; i < articulosUnicos.length; i++) {

        opt = document.createElement('option');
        opt.value = i;
        opt.id = "opti"
        opt.innerHTML = articulosUnicos[i];
        opSelect.appendChild(opt);
    }

    cargarEstados();
}

function crearVenta() {

    var nombreComprador = document.getElementById("nombreComprador").value;
    var dniComprador = document.getElementById("dniComprador").value;
    var tlf = document.getElementById("telefonoComprador").value;
    var mail = document.getElementById("mailComprador").value;
    var dirComprador = document.getElementById("direccionComprador").value;

    document.getElementById("dniE").innerHTML = "";
    document.getElementById("nombreE").innerHTML = "";
    document.getElementById("dniE").innerHTML = "";
    document.getElementById("tlfE").innerHTML = "";
    document.getElementById("mailE").innerHTML = "";
    document.getElementById("dirE").innerHTML = "";

    precioVenta = parseFloat(precioVenta);

    var compruebaN = comprobarNombre(nombreComprador);

    if (compruebaN == false) {

        document.getElementById("nombreE").innerHTML = " Nombre de cliente no valido";

    }

    var compruebaD = comprobarDNI(dniComprador);

    if (compruebaD == false) {

        document.getElementById("dniE").innerHTML = " DNI no valido";

    }

    var compruebaT = comprobarTelefono(tlf);

    if (compruebaT == false) {

        document.getElementById("tlfE").innerHTML = " Telefono no valido";

    }

    var compruebaM = comprobarMail(mail);

    if (compruebaM == false) {

        document.getElementById("mailE").innerHTML = " Email no valido";

    }

    if (dirComprador == "") {

        document.getElementById("dirE").innerHTML = " Introduce el la direccion del cliente";
        comprobacion = false;

    }

    if (compruebaN == true && compruebaD == true && compruebaT == true && compruebaM == true) {

        var idEstado = document.getElementById("estadoVenta").value;
        var estado = compras[idEstado].estado;
        var precio = compras[idEstado].precioTotal;
        var precio = parseFloat(precio);
        var cadena = "";
        var precioVenta = parseFloat(precioVenta);
        precioVenta = parseFloat(precioVenta);
        var incrementoEstado;
        var pVenta = parseFloat(pVenta);
        var ivaVenta = parseFloat(ivaVenta);
        var pVentaTotal = parseFloat(pVentaTotal);
        var precioventa = parseFloat(precioventa);

        if (estado == "Bueno") {

            precioventa = precio + (precio * 0.07);
            incrementoEstado = "7%";
        }

        if (estado == "Regular") {

            precioventa = precio + (precio * 0.06);
            incrementoEstado = "6%";

        }
        if (estado == "Malo") {

            precioventa = precio + (precio * 0.05);
            incrementoEstado = "5%";

        }

        ivaVenta = precioventa * 0.21;
        pVentaTotal = precioventa + ivaVenta;

        iVenta = ivaVenta.toFixed(2);
        pVenta = pVentaTotal.toFixed(2);

        document.getElementById("creaVenta").innerHTML = "Completar venta";
        document.getElementById("creaVenta").onclick = function() { finVenta(idEstado, compras[idEstado].tipoArticulo, compras[idEstado].articulo, estado, pVenta, nombreComprador, dniComprador, iVenta, precio, incrementoEstado, tlf, mail, dirComprador) };
        document.getElementById("form1").className = "col-6 col-lg-5 col-md-12 col-sm-12 col-xs-12";

        document.getElementById("datosVenta").style.display = "block";
        document.getElementById("datosVenta").className = "col-2 col-lg-3 col-md-6 col-sm-8 col-xs-8";
        document.getElementById("formularioVenta").style.marginRight = "20%";

        cadena += '<h4 align="center"><img src="imagenes/carrito.png" alt="carrito" width="40" height="50"><b> Datos de la venta</b><h4>';
        cadena += "<hr  size=3><br>";
        cadena += '<div align="center"><p><strong>Categoría: </strong> ' + compras[idEstado].tipoArticulo + "</p>";
        cadena += "<p><strong>Articulo: </strong> " + compras[idEstado].articulo + "</p>";
        cadena += "<p><strong>Precio base: </strong> " + precio + "€</p>";
        cadena += "<p><strong>Estado: </strong>" + estado + "</p>";
        cadena += "<p><strong>Porcentaje del estado: </strong>" + incrementoEstado + "</p>";
        cadena += "<p><strong>Iva: </strong>" + iVenta + "€</p>";
        cadena += "<p><strong>Precio de venta: </strong> " + pVenta + "€</p></div>";
        /*cadena += '<button type="button" id="cancelarCompra" class="btn btn-danger" onclick="cancelarV()">Cancelar</button>';*/
        cadena += '<br><br><button type="button" id="cancelarVenta" class="boton2" onclick="cancelarV()"><div class="default-btn2"><svg class="css-i6dzq1" stroke-linejoin="round" stroke-linecap="round" fill="none" stroke-width="2" stroke="#ffd300" height="20" width="20" viewBox="0 0 24 24"><circle r="1" cy="21" cx="9"></circle><circle r="1" cy="21" cx="20"></circle><path fill="none" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path></svg><span id="creaCompra">Cancelar</span></div></button>';

        document.getElementById("datosVenta").innerHTML = cadena;

    }
}

function cargarEstados() {

    var opArticulo = document.getElementById("artEnVenta").value;
    var nom = articulosUnicos[opArticulo];

    var opEstado = document.getElementById("estadoVenta");

    for (var i = opEstado.options.length - 1; i >= 0; i--) {
        opEstado.remove(i);
    }

    for (var i = 0; i < compras.length; i++) {

        if (nom == compras[i].articulo) {

            opt = document.createElement('option');
            opt.value = i;
            opt.id = "optiArt"
            opt.innerHTML = compras[i].estado;
            opEstado.appendChild(opt);
        }
    }
}

function finVenta(idEstado, tipoArt, nombreArt, estado, precioTotal, nombre, dni, ivaVenta, precioBase, incrementoEstado, tlf, mail, dir) {

    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";
    document.getElementById("confirmacion").style.display = "block";
    document.getElementById("confirm").innerHTML = "<h3>Venta realizada con exito</h3>";

    var nuevaVenta = new venta(tipoArt, nombreArt, estado, precioBase, incrementoEstado, ivaVenta, precioTotal, nombre, dni);
    ventas.push(nuevaVenta);

    var nuevoCliente = new cliente(nombre, dni, tlf, mail, dir);
    clientesTienda.push(nuevoCliente);

    document.getElementById("formVenta").reset();

    localStorage.setItem('ventas', JSON.stringify(ventas));
    localStorage.setItem('cliente', JSON.stringify(clientesTienda));

    compras.splice(idEstado, 1);
    localStorage.setItem('compras', JSON.stringify(compras));

}

/* Mostrar los datos de los articulos que se mantienen en el almacen */

function almacen() {

    document.getElementById("form1").className = "col-9 col-md-9 col-sm-12 col-xs-12";

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("tablaAlmacen").style.display = "block";
    document.getElementById("tablasCierre").style.display = "none";
    document.getElementById("confirmacion").style.display = "none";
    document.getElementById("confirmacionAlmacen").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";

    document.getElementById("formCompra").reset();
    document.getElementById("formVenta").reset();

    var cadenaArticuloss = "";
    cadenaArticuloss += '<thead class="bg-light">';
    cadenaArticuloss += "<tr>";
    cadenaArticuloss += "<th>Tipo de articulo</th>";
    cadenaArticuloss += "<th>Articulo</th>";
    cadenaArticuloss += "<th>Estado</th>";
    cadenaArticuloss += "<th>Nombre vendedor</th>";
    cadenaArticuloss += "<th>Dni vendedor</th>";
    cadenaArticuloss += "<th>Precio base</th>";
    cadenaArticuloss += "<th>Iva</th>";
    cadenaArticuloss += "<th>Precio Compra</th>";
    cadenaArticuloss += "</tr>";
    cadenaArticuloss += '</thead>';
    cadenaArticuloss += '<tbody>';

    for (var i = 0; i < compras.length; i++) {

        var ivaCompraAlmacen = compras[i].iva;
        ivaCompraAlmacen = parseFloat(ivaCompraAlmacen);
        ivaCompraAlmacen = ivaCompraAlmacen.toFixed(2);

        cadenaArticuloss += "<tr>";
        cadenaArticuloss += "<td>" + compras[i].tipoArticulo + "</td>";
        cadenaArticuloss += "<td>" + compras[i].articulo + "</td>";
        cadenaArticuloss += "<td>" + compras[i].estado + "</td>";
        cadenaArticuloss += "<td>" + compras[i].nombre + "</td>";
        cadenaArticuloss += "<td>" + compras[i].dni + "</td>";
        cadenaArticuloss += "<td>" + compras[i].precio + "€</td>";
        cadenaArticuloss += "<td>" + ivaCompraAlmacen + "€</td>";
        cadenaArticuloss += "<td>" + compras[i].precioTotal + "€</td>";
        cadenaArticuloss += "</tr>";
    }
    cadenaArticuloss += '</tbody>';

    document.getElementById("ArticulosAlmacenados").innerHTML = cadenaArticuloss;


    $(document).ready(function() {
        $('#ArticulosAlmacenados').DataTable({
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]
        });
    });
    $("#ArticulosAlmacenados").dataTable().fnDestroy();



}


/* Busqueda de artículos concretos para conocer sus datos (modulo desactivado)

  function buscarArticulos(data){
 
    document.getElementById("contenidoAlmacen").style.display="none";
    document.getElementById("busquedaAlmacen").style.display="block";

    var cadenaAlmacen="";
            cadenaAlmacen+="<tr>";
                cadenaAlmacen+="<th>Tipo de articulo</th>";
                cadenaAlmacen+="<th>Articulo</th>";
                cadenaAlmacen+="<th>Estado</th>";
                cadenaAlmacen+="<th>Nombre del vendedor</th>";
                cadenaAlmacen+="<th>Dni del vendedor</th>";
                cadenaAlmacen+="<th>Precio base</th>";
                cadenaAlmacen+="<th>Iva</th>";
                cadenaAlmacen+="<th>Precio Compra</th>";
            cadenaAlmacen+="</tr>";

    compras.forEach(articulos =>{
        if(articulos.articulo.includes(data)){

            var ivaCompraAlmacen=articulos.iva;
            ivaCompraAlmacen=parseFloat(ivaCompraAlmacen);
            ivaCompraAlmacen=ivaCompraAlmacen.toFixed(2);

            cadenaAlmacen+="<tr>";
                cadenaAlmacen+="<td>"+articulos.tipoArticulo+"</td>";
                cadenaAlmacen+="<td>"+articulos.articulo+"</td>";
                cadenaAlmacen+="<td>"+articulos.estado+"</td>";
                cadenaAlmacen+="<td>"+articulos.nombre+"</td>";
                cadenaAlmacen+="<td>"+articulos.dni+"</td>";
                cadenaAlmacen+="<td>"+articulos.precio+"€</td>";
                cadenaAlmacen+="<td>"+ivaCompraAlmacen+"€</td>";
                cadenaAlmacen+="<td>"+articulos.precioTotal+"€</td>";
            cadenaAlmacen+="</tr>";
        }

        if(articulos.tipoArticulo.includes(data)){

            var ivaCompraAlmacen=articulos.iva;
            ivaCompraAlmacen=parseFloat(ivaCompraAlmacen);
            ivaCompraAlmacen=ivaCompraAlmacen.toFixed(2);

            cadenaAlmacen+="<tr>";
                cadenaAlmacen+="<td>"+articulos.tipoArticulo+"</td>";
                cadenaAlmacen+="<td>"+articulos.articulo+"</td>";
                cadenaAlmacen+="<td>"+articulos.estado+"</td>";
                cadenaAlmacen+="<td>"+articulos.nombre+"</td>";
                cadenaAlmacen+="<td>"+articulos.dni+"</td>";
                cadenaAlmacen+="<td>"+articulos.precio+"€</td>";
                cadenaAlmacen+="<td>"+ivaCompraAlmacen+"€</td>";
                cadenaAlmacen+="<td>"+articulos.precioTotal+"€</td>";
            cadenaAlmacen+="</tr>";
        }
    });

    document.getElementById("Busqueda").innerHTML=cadenaAlmacen;
     
  }

  */

/* Cargar almacen desde la base de datos */

function cargaAlmacen() {

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("tablaAlmacen").style.display = "none";
    document.getElementById("tablasCierre").style.display = "none";
    document.getElementById("confirmacion").style.display = "none";
    document.getElementById("confirmacionAlmacen").style.display = "block";
    document.getElementById("datosCompra").style.display = "none";

    document.getElementById("formCompra").reset();
    document.getElementById("formVenta").reset();

    $.ajax({
        type: "POST",
        url: "php/almacen.php",
        dataType: "json",

    })

    .done(function(data) {

            compras = compras.concat(data);

        })
        .fail(function() {
            console.log("no");
        });

    document.getElementById("confirmAlm").innerHTML = "<h3>Almacen descargado</h3>";
}

/* Restaurar los valores almacenados en localstorage */

function restaurar() {

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("tablaAlmacen").style.display = "none";
    document.getElementById("tablasCierre").style.display = "none";
    document.getElementById("confirmacion").style.display = "block";
    document.getElementById("confirmacionAlmacen").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";

    document.getElementById("formCompra").reset();
    document.getElementById("formVenta").reset();

    if (localStorage.getItem('compras') !== null) {

        compras1 = JSON.parse(localStorage.getItem('compras'));
        compras = compras.concat(compras1);
    }
    if (localStorage.getItem('ventas') !== null) {

        ventas1 = JSON.parse(localStorage.getItem('ventas'));
        ventas = ventas.concat(ventas1);
    }
    if (localStorage.getItem('respaldoCompras') !== null) {

        respaldoCompras = JSON.parse(localStorage.getItem('respaldoCompras'));
    }

    document.getElementById("confirm").innerHTML = "<h3>Informacion restaurada</h3>";

}

/* Funciones para comprobar el nombre y Dni de los formularios */

function comprobarNombre(nombre) {

    var patronNombre = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    var compNom = true;

    compNombre = patronNombre.test(nombre);

    if (compNombre == false) {

        compNom = false;
    }

    return compNom;
}

function comprobarTelefono(telefono) {

    var str = telefono.toString().replace(/\s/g, '');
    return str.length === 9 && /^[679]{1}[0-9]{8}$/.test(str);

}

function comprobarMail(mail) {

    var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return String(mail).search(filter) != -1;

}

function comprobarDNI(dni) {

    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var compDni = true;

    if (dni.length == 9) {

        var numDNI = parseInt(dni.substring(0, 8));
        var letraDNI = dni.substring(8, 9);

        if (isNaN(numDNI)) {

            document.getElementById("dniError").innerHTML = " Formato de DNI invalido";
            compDni = false;
        } else {

            var letraCorrecta = letras[numDNI % 23];

            if (letraDNI.toUpperCase() != letraCorrecta) {

                document.getElementById("dniError").innerHTML = " Formato de DNI invalido";
                compDni = false;

            }
        }

    } else {

        document.getElementById("dniError").innerHTML = " Longitud del DNI invalida";
        compDni = false;
    }

    return compDni;
}


/* Funciones para resetear los formularios en caso de anular la compra/venta */

function cancelarC() {

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";

    document.getElementById("formCompra").reset();

    document.getElementById("confirmacion").style.display = "block";
    document.getElementById("confirm").innerHTML = "<h3>Se ha cancelado la compra</h3>";

}

function cancelarV() {

    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";

    document.getElementById("formVenta").reset();

    document.getElementById("confirmacion").style.display = "block";
    document.getElementById("confirm").innerHTML = "<h3>Se ha cancelado la venta</h3>";

}

/* Mostrar los datos almacenados en las tablas de cierre antes de enviarlos al servidor */

function cierre() {

    document.getElementById("form1").className = "col-9 col-md-9 col-sm-12 col-xs-12";

    document.getElementById("formularioCompra").style.display = "none";
    document.getElementById("formularioVenta").style.display = "none";
    document.getElementById("tablaAlmacen").style.display = "none";
    document.getElementById("tablasCierre").style.display = "block";
    document.getElementById("confirmacion").style.display = "none";
    document.getElementById("confirmacionAlmacen").style.display = "none";
    document.getElementById("datosCompra").style.display = "none";
    document.getElementById("datosVenta").style.display = "none";

    document.getElementById("formCompra").reset();
    document.getElementById("formVenta").reset();

    balance.length = 0;

    var cadenaCompras = "";
    var cadenaVentas = "";
    var gastoTotal = 0;
    var gananciaTotal = 0;
    var recaudacion;

    cadenaCompras += '<thead class="bg-light">';
    cadenaCompras += "<tr>";
    cadenaCompras += "<th>Tipo de articulo</th>";
    cadenaCompras += "<th>Articulo</th>";
    cadenaCompras += "<th>Estado</th>";
    cadenaCompras += "<th>Nombre del vendedor</th>";
    cadenaCompras += "<th>Dni del vendedor</th>";
    cadenaCompras += "<th>Precio base</th>";
    cadenaCompras += "<th>Iva</th>";
    cadenaCompras += "<th>Precio Compra</th>";
    cadenaCompras += "</tr>";
    cadenaCompras += '</thead>';
    cadenaCompras += '<tbody>';

    for (var i = 0; i < respaldoCompras.length; i++) {

        var ivaCompra = respaldoCompras[i].iva;
        ivaCompra = parseFloat(ivaCompra);
        ivaCompra = ivaCompra.toFixed(2);


        cadenaCompras += "<tr>";
        cadenaCompras += "<td>" + respaldoCompras[i].tipoArticulo + "</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].articulo + "</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].estado + "</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].nombre + "</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].dni + "</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].precio + "€</td>";
        cadenaCompras += "<td>" + ivaCompra + "€</td>";
        cadenaCompras += "<td>" + respaldoCompras[i].precioTotal + "€</td>";
        cadenaCompras += "</tr>";

        gastoTotal += respaldoCompras[i].precioTotal;
        gastoTotal = parseFloat(gastoTotal);
    }
    cadenaCompras += '</tbody>';
    cadenaCompras += '<tbody>';
    cadenaCompras += "<tr>";
    cadenaCompras += "<td><strong>TOTAL COMPRAS</strong></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td></td>";
    cadenaCompras += "<td><strong>" + gastoTotal.toFixed(2) + "€</strong></td>";
    cadenaCompras += "</tr>";
    cadenaCompras += '</tbody>';

    document.getElementById("tablaCompras").innerHTML = cadenaCompras;

    cadenaVentas += '<thead class="bg-light">';
    cadenaVentas += "<tr>";
    cadenaVentas += "<th>Tipo de articulo</th>";
    cadenaVentas += "<th>Articulo</th>";
    cadenaVentas += "<th>Estado</th>";
    cadenaVentas += "<th>Nombre vendedor</th>";
    cadenaVentas += "<th>Dni vendedor</th>";
    cadenaVentas += "<th>Precio base</th>";
    cadenaVentas += "<th>Porcentaje incremento</th>";
    cadenaVentas += "<th>Iva</th>";
    cadenaVentas += "<th>Precio Venta</th>";
    cadenaVentas += "</tr>";
    cadenaVentas += '</thead>';
    cadenaVentas += '<tbody>';

    for (var i = 0; i < ventas.length; i++) {

        cadenaVentas += "<tr>";
        cadenaVentas += "<td>" + ventas[i].tipoArt + "</td>";
        cadenaVentas += "<td>" + ventas[i].nombreArt + "</td>";
        cadenaVentas += "<td>" + ventas[i].estado + "</td>";
        cadenaVentas += "<td>" + ventas[i].nombre + "</td>";
        cadenaVentas += "<td>" + ventas[i].dni + "</td>";
        cadenaVentas += "<td>" + ventas[i].precioBase + "€</td>";
        cadenaVentas += "<td>" + ventas[i].incremento + "</td>";
        cadenaVentas += "<td>" + ventas[i].ivaVenta + "€</td>";
        cadenaVentas += "<td>" + ventas[i].precioVenta + "€</td>";
        cadenaVentas += "</tr>";

        gananciaTotal += parseFloat(ventas[i].precioVenta);
    }
    cadenaVentas += '</tbody>';
    cadenaVentas += '<tbody>';
    cadenaVentas += "<tr>";
    cadenaVentas += "<td><strong>TOTAL VENTAS</strong></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td></td>";
    cadenaVentas += "<td><strong>" + gananciaTotal + "€</strong></td>";
    cadenaVentas += "</tr>";
    cadenaVentas += '</tbody>';

    document.getElementById("tablaVentas").innerHTML = cadenaVentas;


    recaudacion = gananciaTotal - gastoTotal;
    recaudacion = recaudacion.toFixed(2);
    recaudacion = parseFloat(recaudacion);

    balance.push(gastoTotal, gananciaTotal, recaudacion);

    if (recaudacion >= 0) {

        document.getElementById("resultadoFinal").className = "positivo";
    } else {

        document.getElementById("resultadoFinal").className = "negativo";
    }

    document.getElementById("resultadoFinal").innerHTML = " El bance diario es de: " + recaudacion + " €";

    $(document).ready(function() {
        $('#tablaCompras').DataTable({
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]
        });
        $('#tablaVentas').DataTable({
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]
        });
    });
    $("#tablaCompras").dataTable().fnDestroy();
    $("#tablaVentas").dataTable().fnDestroy();

}

/* Obtencion de los datos almacenados en localstorage pasandolos a JSON para enviarlos mediante Ajax a un archivo PHP que se encargara de recogerlo y subirlo al servidor */

function finCaja() {


    var v = JSON.parse(localStorage.getItem('ventas'));
    var c = JSON.parse(localStorage.getItem('compras'));
    var r = JSON.parse(localStorage.getItem('respaldoCompras'));
    var cli = JSON.parse(localStorage.getItem('cliente'));


    var datos1 = JSON.stringify(v);
    var datos2 = JSON.stringify(r);
    var datos3 = JSON.stringify(c);
    var datos4 = JSON.stringify(balance);
    var datos5 = JSON.stringify(cli);

    $.ajax({
        type: "POST",
        url: "php/finDia.php",
        data: { data1: datos1, data2: datos2, data3: datos3, data4: datos4, data5: datos5 },

    })

    .done(function(data) {
            //$('#output').html(data);
            localStorage.clear();
            window.location.reload();
        })
        .fail(function() {
            console.log("no");
        });


}

/* Funcion para conectar con un archivo PHP mediante Ajax,el cual se encargara de cerrar las sessiones abiertas y devolvernos a la pantalla de logueo */

$(document).ready(function() {

    $('.logout').click(function() {

        $.ajax({
                type: "POST",
                cache: false,
                url: "php/logout.php",
                datatype: "html",
                data: $('.logout').serialize()
            })
            .done(function() {

                window.location.href = "php/login.php"
            })

        .fail(function() {
            console.log("Fallo en ejecucion")
        });
    });
});