
    var usuarios= new Array();
    var usuNombre = new Array();

    $.ajax({
        type: "POST",
        url: "../php/tablaUsuarios.php",
        dataType: "json",
        
    })

    .done (function(data){

        usuarios = data;
        
        var cadenaUsuarios="";

        cadenaUsuarios+='<thead class="bg-light">';
            cadenaUsuarios+='<tr class="border-0">';
                cadenaUsuarios+='<th class="border-0">Nombre</th>';
                cadenaUsuarios+='<th class="border-0">Rol</th>';
                cadenaUsuarios+='<th class="border-0">Estado</th>';
                cadenaUsuarios+='<th class="border-0">Eliminar</th>';
            cadenaUsuarios+='</tr>';
        cadenaUsuarios+='</thead>';
        cadenaUsuarios+='<tbody>';

        for(var i=0; i<usuarios.length; i++){

            var clase="";

            if(usuarios[i].estado=="Conectado"){

                clase="badge-dot badge-success mr-1";
            }else{
                clase="badge-dot badge-danger";
            }

            cadenaUsuarios+='<tr>';
                cadenaUsuarios+='<td>'+usuarios[i].nombre+'</td>';
                cadenaUsuarios+='<td>'+usuarios[i].rol+'</td>';
                cadenaUsuarios+='<td><span class="'+clase+'"></span>'+usuarios[i].estado+'</td>';
                cadenaUsuarios+='<td><input type="checkbox" id="nbox'+i+'"value="'+usuarios[i].nombre+'"></td>';
            cadenaUsuarios+='</tr>';
        }
        cadenaUsuarios+='</tbody>';

        document.getElementById("EliminarU").innerHTML=cadenaUsuarios;

        $(document).ready(function() {
            $('#EliminarU').DataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            } );
        } );

    })
    .fail (function() {
        console.log("no");
    });


        $(document).ready(function(){

            $('.logout').click(function(){
    
            $.ajax({
                    type: "POST", 
                    cache: false, 
                    url: "../php/logout.php", 
                    datatype: "html", 
                    data: $('.logout').serialize()
                    }) 
                    .done(function () {
    
                        window.location.href ="../php/login.php";  
                    })
    
                .fail(function () {
                            console.log("Fallo en ejecucion"); 
                        });
            });
        });

    function eliminarUsuario(){
    
        $('#EliminarU input[type=checkbox]').each(function(){
            if (this.checked) {
                usuNombre.push($(this).val());
            }
        }); 

        eliminarUsu();

    }

    function eliminarUsu(){
        console.log(usuNombre);
        var datos1 = JSON.stringify(usuNombre);

        $.ajax({
            type: "POST",
            url: "../php/eliminarUsuarios.php",
            data: { data1: datos1},
            
        })

        .done (function(data) {
            //$('#output').html(data);
            console.log(data);
            window.location.href ="eliminar_usuarios.php"; 
            document.getElementById("exitoEliminarUsu").innerHTML="Uusarios eliminados con exito";

        })
        .fail (function() {
            console.log("no");
        });

    }