var CompraVenta= new Array();
var clientes= new Array();
var vendedores= new Array();
var balances= new Array();
var categorias= new Array();
var compradores= new Array();
var categoria1;



    // ============================================================== 
    // Product Category
    // ============================================================== 

    $.ajax({
        type: "POST",
        url: "../php/categoriaCompras.php",
        dataType: "json",
        
    })

    .done (function(dataCat){
        categorias=dataCat;

        var chart = new Chartist.Pie('.ct-chart-category', {
            series: [categorias[0],categorias[1],categorias[2],categorias[3],categorias[4],categorias[5],categorias[6]],
            labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
        }, {
            donut: true,
            showLabel: false,
            donutWidth: 40
    
        });

        chart.on('draw', function(data) {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();
    
                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });
    
                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze'
                    }
                };
    
                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                }
    
                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px'
                });
    
                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });
    })
    .fail (function() {
        console.log("no");
    });


    var chart = new Chartist.Pie('.ct-chart-category', {
        series: [2,3,4,5],
        labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
    }, {
        donut: true,
        showLabel: false,
        donutWidth: 40

    });

    chart.on('draw', function(data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    


    // ============================================================== 
    // Customer acquisition
    // ============================================================== 

   

    chart.on('draw', function(data) {
        if (data.type === 'line' || data.type === 'area') {
            data.element.animate({
                d: {
                    begin: 2000 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
    });




    // ============================================================== 
    // Revenue Cards
    // ============================================================== 
    
    $.ajax({
        type: "POST",
        url: "../php/balanceCompras.php",
        dataType: "json",
        
    })

    .done (function(dataC){
        balancesC=dataC;

        $("#sparkline-revenue").sparkline([balancesC[11],balancesC[10],balancesC[9],balancesC[8],balancesC[7],balancesC[6],balancesC[5],balancesC[4],balancesC[3],balancesC[2],balancesC[1],balancesC[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        var Bcompras_redondeado=balancesC[12].toFixed(2);
        document.getElementById("totalC").innerHTML=Bcompras_redondeado+"€";

    })
    .fail (function() {
        console.log("no");
    });
    


    // ============================================================== 
    // Revenue By Categories
    // ============================================================== 

    $(document).ready(function(){

        $('.logout').click(function(){

        $.ajax({
                type: "POST", 
                cache: false, 
                url: "../php/logout.php", 
                datatype: "html", 
                data: $('.logout').serialize()
                }) 
                .done(function () {

                    window.location.href ="../php/login.php"   
                })

            .fail(function () {
                        console.log("Fallo en ejecucion") 
                    });
        });
    });


    $(document).ready(function(){

        $.ajax({
            type: "POST",
            url: "../php/ultimascompras.php",
            dataType: "json",
            
        })

        .done (function(data){

            CompraVenta = data;
            
            var cadenaArticuloss="";


            cadenaArticuloss+='<thead class="bg-light">';
                cadenaArticuloss+='<tr class="border-0">';
                    cadenaArticuloss+='<th class="border-0">#</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo de artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Estado</th>';
                    cadenaArticuloss+='<th class="border-0">Precio Base</th>';
                    cadenaArticuloss+='<th class="border-0">Iva</th>';
                    cadenaArticuloss+='<th class="border-0">Precio compra</th>';
                    cadenaArticuloss+='<th class="border-0">Nombre</th>';
                    cadenaArticuloss+='<th class="border-0">Dni</th>';
                    cadenaArticuloss+='<th class="border-0">Fecha</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo</th>';
                cadenaArticuloss+='</tr>';
            cadenaArticuloss+='</thead>';
            cadenaArticuloss+='<tbody>';

            for(var i=0; i<CompraVenta.length; i++){

                var clase="";

                if(CompraVenta[i].tipo=="compra"){

                    clase="badge-dot badge-brand mr-1";
                }else{
                    clase="badge-dot badge-success mr-1";
                }

                cadenaArticuloss+='<tr>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].id+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].tipoArticulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].articulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].estado+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precio+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].iva+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precioTotal+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].nombre+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].dni+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].fecha+'</td>';
                    cadenaArticuloss+='<td><span class="'+clase+'"></span>'+CompraVenta[i].tipo+'</td>';
                cadenaArticuloss+='</tr>';
            }

            cadenaArticuloss+='</tbody>';

            document.getElementById("tablaArticulos").innerHTML=cadenaArticuloss;

        })
        .fail (function() {
            console.log("no");
        });

    });


    // ============================================================== 
    // Numero cliente/compras/ventas
    // ============================================================== 
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "../php/recuento_compras.php",
            dataType: "json",
            
        })

        .done (function(recuentoTotal){
            recuento=recuentoTotal;

            document.getElementById("totalClientes").innerHTML=recuento[0]+" clientes";
            document.getElementById("totalCompras").innerHTML=recuento[1] +" compras";
            document.getElementById("totalProductos").innerHTML=recuento[2]+ " productos";

        })
        .fail (function() {
            console.log("no");
        });

    });


    $.ajax({
        type: "POST",
        url: "../php/tablaCompradores.php",
        dataType: "json",
        
    })

    .done (function(data){

        compradores = data;
        var cadenaClientes="";


        cadenaClientes+='<thead class="bg-light">';
            cadenaClientes+='<tr class="border-0">';
                cadenaClientes+='<th class="border-0">Nombre</th>';
                cadenaClientes+='<th class="border-0">Telefono</th>';
                cadenaClientes+='<th class="border-0">Mail</th>';
                cadenaClientes+='<th class="border-0">Direccion</th>';
                cadenaClientes+='<th class="border-0">Dni</th>';
                cadenaClientes+='<th class="border-0">Articulos vendidos</th>';
                cadenaClientes+='<th class="border-0">Cantidad obtenida</th>';
            cadenaClientes+='</tr>';
        cadenaClientes+='</thead>';
        cadenaClientes+='<tbody>';

        for(var i=0; i<compradores.length; i++){

            cadenaClientes+='<tr>';
                cadenaClientes+='<td>'+compradores[i].nom+'</td>';
                cadenaClientes+='<td>'+compradores[i].telf+'</td>';
                cadenaClientes+='<td>'+compradores[i].mail+'</td>';
                cadenaClientes+='<td>'+compradores[i].dir+'</td>';
                cadenaClientes+='<td>'+compradores[i].dni+'</td>';
                cadenaClientes+='<td>'+compradores[i].veces+' ventas</td>';
                cadenaClientes+='<td>'+compradores[i].total+'€</td>';
            cadenaClientes+='</tr>';
        }
        cadenaClientes+='</tbody>';

        document.getElementById("tablaCompradores").innerHTML=cadenaClientes;

        $(document).ready(function() {
            $('#tablaCompradores').DataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            } );
        } );

    })
    .fail (function() {
        console.log("no");
    });


    