
    var tablasTransacciones= new Array();

        $.ajax({
            type: "POST",
            url: "../php/tablaTransacciones.php",
            dataType: "json",
            
        })

        .done (function(data){

            tablasTransacciones = data;
            
            var cadenaTransacciones="";


            cadenaTransacciones+='<thead class="bg-light">';
                cadenaTransacciones+='<tr class="border-0">';
                    cadenaTransacciones+='<th class="border-0">#</th>';
                    cadenaTransacciones+='<th class="border-0">Tipo de artículo</th>';
                    cadenaTransacciones+='<th class="border-0">Artículo</th>';
                    cadenaTransacciones+='<th class="border-0">Estado</th>';
                    cadenaTransacciones+='<th class="border-0">Precio Base</th>';
                    cadenaTransacciones+='<th class="border-0">Precio compra/venta</th>';
                    cadenaTransacciones+='<th class="border-0">Nombre</th>';
                    cadenaTransacciones+='<th class="border-0">Dni</th>';
                    cadenaTransacciones+='<th class="border-0">Tipo</th>';
                    cadenaTransacciones+='<th class="border-0">Fecha</th>';
                cadenaTransacciones+='</tr>';
            cadenaTransacciones+='</thead>';
            cadenaTransacciones+='<tbody>';

            for(var i=0; i<tablasTransacciones.length; i++){

                var clase="";

                if(tablasTransacciones[i].tipo=="compra"){

                    clase="badge-dot badge-brand mr-1";
                }else{
                    clase="badge-dot badge-success mr-1";
                }

                cadenaTransacciones+='<tr>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].id+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].tipoArticulo+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].articulo+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].estado+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].precio+'€</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].precioTotal+'€</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].nombre+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].dni+'</td>';
                    cadenaTransacciones+='<td><span class="'+clase+'"></span>'+tablasTransacciones[i].tipo+'</td>';
                    cadenaTransacciones+='<td>'+tablasTransacciones[i].fecha+'</td>';
                cadenaTransacciones+='</tr>';
            }
            cadenaTransacciones+='</tbody>';

            document.getElementById("TransaccionesTabla").innerHTML=cadenaTransacciones;

            $(document).ready(function() {
                $('#TransaccionesTabla').DataTable( {
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                } );
            } );

        })
        .fail (function() {
            console.log("no");
        });


        $(document).ready(function(){

            $('.logout').click(function(){
    
            $.ajax({
                    type: "POST", 
                    cache: false, 
                    url: "../php/logout.php", 
                    datatype: "html", 
                    data: $('.logout').serialize()
                    }) 
                    .done(function () {
    
                        window.location.href ="../php/login.php"   
                    })
    
                .fail(function () {
                            console.log("Fallo en ejecucion") 
                        });
            });
        });