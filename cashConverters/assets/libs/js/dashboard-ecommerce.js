var CompraVenta= new Array();
var clientes= new Array();
var vendedores= new Array();
var balances= new Array();
var categorias= new Array();
var categoria1;




    // ============================================================== 
    // Product Category
    // ============================================================== 

    

    $.ajax({
        type: "POST",
        url: "../php/categoriaProductos.php",
        dataType: "json",
        
    })

    .done (function(dataCat){
        categorias=dataCat;

        var chart = new Chartist.Pie('.ct-chart-category', {
            series: [categorias[0],categorias[1],categorias[2],categorias[3],categorias[4],categorias[5],categorias[6]],
            labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
        }, {
            donut: true,
            showLabel: false,
            donutWidth: 40
    
        });

        chart.on('draw', function(data) {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();
    
                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });
    
                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze'
                    }
                };
    
                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                }
    
                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px'
                });
    
                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });
    })
    .fail (function() {
        console.log("no");
    });


    var chart = new Chartist.Pie('.ct-chart-category', {
        series: [2,3,4,5],
        labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
    }, {
        donut: true,
        showLabel: false,
        donutWidth: 40

    });

    chart.on('draw', function(data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    


    // ============================================================== 
    // Customer acquisition
    // ============================================================== 

    $.ajax({
        type: "POST",
        url: "../php/compra_venta.php",
        dataType: "json",
        
    })

    .done (function(data){
        datos=data;

        var chart = new Chartist.Line('.ct-chart', {
            labels: ['Lun', 'Mar', 'Mier', 'Jue', 'Vier'],
            series: [
                [datos[0], datos[1], datos[2], datos[3], datos[4]],
                [datos[5], datos[6], datos[7], datos[8], datos[9]]

            ]
        }, {
            low: 0,
            showArea: true,
            showPoint: false,
            fullWidth: true
        });

    })
    .fail (function() {
        console.log("no");
    });

    chart.on('draw', function(data) {
        if (data.type === 'line' || data.type === 'area') {
            data.element.animate({
                d: {
                    begin: 2000 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
    });




    // ============================================================== 
    // Revenue Cards
    // ============================================================== 
    
    $.ajax({
        type: "POST",
        url: "../php/balanceCompras.php",
        dataType: "json",
        
    })

    .done (function(dataC){
        balancesC=dataC;

        $("#sparkline-revenue").sparkline([balancesC[11],balancesC[10],balancesC[9],balancesC[8],balancesC[7],balancesC[6],balancesC[5],balancesC[4],balancesC[3],balancesC[2],balancesC[1],balancesC[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        var Bcompras_redondeado=balancesC[12].toFixed(2);
        document.getElementById("totalC").innerHTML=Bcompras_redondeado+"€";

    })
    .fail (function() {
        console.log("no");
    });
    

    $.ajax({
        type: "POST",
        url: "../php/balanceVentas.php",
        dataType: "json",
        
    })

    .done (function(dataV){
        balancesV=dataV;

        $("#sparkline-revenue2").sparkline([balancesV[11],balancesV[10],balancesV[9],balancesV[8],balancesV[7],balancesV[6],balancesV[5],balancesV[4],balancesV[3],balancesV[2],balancesV[1],balancesV[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        var Bventas_redondeado=balancesV[12].toFixed(2);
        document.getElementById("totalV").innerHTML=Bventas_redondeado+"€";

    })
    .fail (function() {
        console.log("no");
    });



    $.ajax({
        type: "POST",
        url: "../php/balanceDiario.php",
        dataType: "json",
        
    })

    .done (function(dataD){
        balancesD=dataD;

        $("#sparkline-revenue3").sparkline([balancesD[13],balancesD[12],balancesD[11],balancesD[10],balancesD[9],balancesD[8],balancesD[7],balancesD[6],balancesD[5],balancesD[4],balancesD[3],balancesD[2],balancesD[1],balancesD[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        num = balancesD[14].toFixed(2);
        document.getElementById("totalD").innerHTML=num+"€";

    })
    .fail (function() {
        console.log("no");
    });



    $.ajax({
        type: "POST",
        url: "../php/balanceTotal.php",
        dataType: "json",
        
    })

    .done (function(dataT){
        balancesT=dataT;

        $("#sparkline-revenue4").sparkline([balancesT[11],balancesT[10],balancesT[9],balancesT[8],balancesT[7],balancesT[6],balancesT[5],balancesT[4],balancesT[3],balancesT[2],balancesT[1],balancesT[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        num = balancesT[12].toFixed(2);
        document.getElementById("totalT").innerHTML=num+"€";

    })
    .fail (function() {
        console.log("no");
    });



    // ============================================================== 
    // Total Revenue
    // ============================================================== 
   



    // ============================================================== 
    // Revenue By Categories
    // ============================================================== 


    $(document).ready(function(){

        $('.logout').click(function(){

        $.ajax({
                type: "POST", 
                cache: false, 
                url: "../php/logout.php", 
                datatype: "html", 
                data: $('.logout').serialize()
                }) 
                .done(function () {

                    window.location.href ="../php/login.php"   
                })

            .fail(function () {
                        console.log("Fallo en ejecucion") 
                    });
        });
    });


    $(document).ready(function(){

        $.ajax({
            type: "POST",
            url: "../php/ultimastransacciones.php",
            dataType: "json",
            
        })

        .done (function(data){

            CompraVenta = data;
            
            var cadenaArticuloss="";


            cadenaArticuloss+='<thead class="bg-light">';
                cadenaArticuloss+='<tr class="border-0">';
                    cadenaArticuloss+='<th class="border-0">#</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo de artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Estado</th>';
                    cadenaArticuloss+='<th class="border-0">Precio Base</th>';
                    cadenaArticuloss+='<th class="border-0">Precio compra/venta</th>';
                    cadenaArticuloss+='<th class="border-0">Nombre</th>';
                    cadenaArticuloss+='<th class="border-0">Dni</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo</th>';
                cadenaArticuloss+='</tr>';
            cadenaArticuloss+='</thead>';
            cadenaArticuloss+='<tbody>';

            for(var i=0; i<CompraVenta.length; i++){

                var clase="";

                if(CompraVenta[i].tipo=="compra"){

                    clase="badge-dot badge-brand mr-1";
                }else{
                    clase="badge-dot badge-success mr-1";
                }

                cadenaArticuloss+='<tr>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].id+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].tipoArticulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].articulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].estado+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precio+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precioTotal+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].nombre+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].dni+'</td>';
                    cadenaArticuloss+='<td><span class="'+clase+'"></span>'+CompraVenta[i].tipo+'</td>';
                cadenaArticuloss+='</tr>';
            }

            cadenaArticuloss+='</tbody>';

            document.getElementById("tablaArticulos").innerHTML=cadenaArticuloss;

        })
        .fail (function() {
            console.log("no");
        });

    });

    // ============================================================== 
    // Top clientes
    // ==============================================================


    $(document).ready(function(){

        $.ajax({
            type: "POST",
            url: "../php/top_clientes.php",
            dataType: "json",
            
        })

        .done (function(dataTop){

            clientes = dataTop;
            
            var cadenaClientes="";

            cadenaClientes+='<thead class="bg-light">';
                cadenaClientes+='<tr class="border-0">';
                    cadenaClientes+='<th class="border-0">Dni</th>';
                    cadenaClientes+='<th class="border-0">Artículos comprados</th>';
                    cadenaClientes+='<th class="border-0">Cantidad gastada</th>';
                    cadenaClientes+='</tr>';
                cadenaClientes+='</thead>';
            cadenaClientes+='<tbody>';

            for(var i=0; i<clientes.length; i++){

                cadenaClientes+='<tr>';
                    cadenaClientes+='<td>'+clientes[i].dni+'</td>';
                    cadenaClientes+='<td>'+clientes[i].veces+'</td>';
                    cadenaClientes+='<td>'+clientes[i].total+'€</td>';
                cadenaClientes+='</tr>';
            }

            cadenaClientes+='</tbody>';

            document.getElementById("tablaTop").innerHTML=cadenaClientes;

        })
        .fail (function() {
            console.log("no");
        });

    });


    $(document).ready(function(){

        $.ajax({
            type: "POST",
            url: "../php/top_vendedores.php",
            dataType: "json",
            
        })

        .done (function(dataVendedores){

            vendedores = dataVendedores;
            
            var cadenaVendedores="";


            cadenaVendedores+='<thead class="bg-light">';
                cadenaVendedores+='<tr class="border-0">';
                    cadenaVendedores+='<th class="border-0">Dni</th>';
                    cadenaVendedores+='<th class="border-0">Artículos vendidos</th>';
                    cadenaVendedores+='<th class="border-0">Cantidad obtenida</th>';
                    cadenaVendedores+='</tr>';
                cadenaVendedores+='</thead>';
            cadenaVendedores+='<tbody>';

            for(var i=0; i<vendedores.length; i++){

                cadenaVendedores+='<tr>';
                    cadenaVendedores+='<td>'+vendedores[i].dni+'</td>';
                    cadenaVendedores+='<td>'+vendedores[i].veces+'</td>';
                    cadenaVendedores+='<td>'+vendedores[i].total+'€</td>';
                cadenaVendedores+='</tr>';
            }

            cadenaVendedores+='</tbody>';

            document.getElementById("tablaVendedores").innerHTML=cadenaVendedores;

        })
        .fail (function() {
            console.log("no");
        });

    });


    // ============================================================== 
    // Numero cliente/compras/ventas
    // ============================================================== 
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "../php/recuento.php",
            dataType: "json",
            
        })

        .done (function(recuentoTotal){
            recuento=recuentoTotal;

            document.getElementById("totalClientes").innerHTML=recuento[0]+" clientes";
            document.getElementById("totalCompras").innerHTML=recuento[2] +" compras";
            document.getElementById("totalVentas").innerHTML=recuento[1]+ " Ventas";
            document.getElementById("totalProductos").innerHTML=recuento[3]+ " productos";

        })
        .fail (function() {
            console.log("no");
        });

    });












