var articulo=new Array();
var clientenuevo=new Array();
var nuevoUsuario=new Array();

/* Funciones para añadir nuevos articulos */


function anadirProducto(){

    var comprobacion=true;

    var tipoArt=document.getElementById("tipoArt").value;
    var nombreArt=document.getElementById("nombreArt").value;
    var estado=document.getElementById("estado").value;
    var precio=document.getElementById("precio").value;
    precio=parseFloat(precio);
    var nombre=document.getElementById("nombre").value;
    var dni=document.getElementById("dni").value;
    var tlf=document.getElementById("telefono").value;
    var mail=document.getElementById("mail").value;
    var dir=document.getElementById("direccion").value;

    document.getElementById("dniError").innerHTML="";
    document.getElementById("nombreError").innerHTML="";
    document.getElementById("precioError").innerHTML="";
    document.getElementById("articuloError").innerHTML="";
    document.getElementById("tlfError").innerHTML="";
    document.getElementById("mailError").innerHTML="";
    document.getElementById("dirError").innerHTML="";

    clientenuevo.length = 0;
    articulo.length = 0;

    if(precio!=""){

        if(precio>=1001 || precio<=0){

            document.getElementById("precioError").innerHTML=" El precio debe ser entre 0 y 1000 €";
            comprobacion=false;
        }

    }
    if(isNaN(precio)){
        document.getElementById("precioError").innerHTML=" Establezca un precio de compra";
        comprobacion=false;
    }

    if(nombreArt==""){

        document.getElementById("articuloError").innerHTML=" Introduce el nombre del articulo";
        comprobacion=false;

    }

    if(dir==""){

        document.getElementById("dirError").innerHTML=" Introduce el la direccion del cliente";
        comprobacion=false;

    }

    var compruebaNombre=comprobarNombre(nombre);

    if(compruebaNombre==false){

        document.getElementById("nombreError").innerHTML=" Nombre de cliente no valido";

    }

    var compruebaDni=comprobarDNI(dni);

    if(compruebaDni==false){

        document.getElementById("dniError").innerHTML=" DNI no valido";

    }

    var compruebatlf=comprobarTelefono(tlf);

    if(compruebatlf==false){

        document.getElementById("tlfError").innerHTML=" Telefono no valido";

    }

    var compruebamail=comprobarMail(mail);

    if(compruebamail==false){

        document.getElementById("mailError").innerHTML=" Email no valido";

    }

    if(comprobacion==true && compruebaNombre==true && compruebaDni==true && compruebatlf==true && compruebamail==true){

        var precTotal=precio+(precio*0.21);
        var iva=precio*0.21;

        articulo.push(tipoArt,nombreArt,estado,precio,iva,precTotal,nombre,dni);

        clientenuevo.push(nombre,dni,tlf,mail,dir);

        conexionServidorArticulos();
    }

  }

function conexionServidorArticulos(){

    var datos1 = JSON.stringify(articulo);
    var datos2 = JSON.stringify(clientenuevo);

    $.ajax({
        type: "POST",
        url: "../php/anadirProductos.php",
        data: { data1: datos1, data2: datos2},
        
    })

    .done (function(data) {
        //$('#output').html(data);
        document.getElementById("basicform").reset();
        document.getElementById("exitoAnadirProd").innerHTML="Articulo añadido con exito al almacen";

    })
    .fail (function() {
        console.log("no");
    });
}
/* Funciones para añadir nuevos usuarios */


function anadirUsuario(){

    var comprobacion=true;

    var usuNom=document.getElementById("nombreUsu").value;
    var usuPass=document.getElementById("pass").value;
    var usuRol=document.getElementById("rol").value;

    document.getElementById("UsuError").innerHTML="";
    document.getElementById("passError").innerHTML="";

    if(usuPass==""){

        document.getElementById("passError").innerHTML=" Introduce la contraseña";
        comprobacion=false;

    }

    var compruebaNombre=comprobarNombre(usuNom);

    if(compruebaNombre==false){

        document.getElementById("UsuError").innerHTML=" Nombre de usuario no valido";

    }

    if(comprobacion==true && compruebaNombre==true ){

        nuevoUsuario.push(usuNom,usuPass,usuRol);

        conexionServidorUsuarios()
    }

}

function conexionServidorUsuarios(){

    var datos1 = JSON.stringify(nuevoUsuario);

    $.ajax({
        type: "POST",
        url: "../php/anadirUsuarios.php",
        data: { data1: datos1},
        
    })

    .done (function(data) {
        //$('#output').html(data);
        document.getElementById("usuform").reset();
        document.getElementById("exitoAnadirUsu").innerHTML="Usuario añadido con exito";

    })
    .fail (function() {
        console.log("no");
    });
}

/* Funciones para comprobar el nombre y Dni de los formularios */

function comprobarNombre(nombre){

    var patronNombre = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    var compNom=true;

    compNombre = patronNombre.test(nombre);
    
    if(compNombre==false){

        compNom=false;
    }

    return compNom;
}

function comprobarTelefono(telefono){

var str = telefono.toString().replace(/\s/g, '');
return str.length === 9 && /^[679]{1}[0-9]{8}$/.test(str);

}

function comprobarMail(mail){

var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
return String(mail).search (filter) != -1;

}

function comprobarDNI(dni){

    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var compDni=true;

    if (dni.length == 9){

        var numDNI = parseInt(dni.substring(0, 8)); 
        var letraDNI=dni.substring(8,9);

        if(isNaN(numDNI)){

            document.getElementById("dniError").innerHTML=" Formato de DNI invalido";
            compDni=false;
         }else{

            var letraCorrecta = letras[numDNI % 23];

            if(letraDNI.toUpperCase() != letraCorrecta){

                document.getElementById("dniError").innerHTML=" Formato de DNI invalido";
                compDni=false;

            }
        }

    }else{

        document.getElementById("dniError").innerHTML=" Longitud del DNI invalida";
        compDni=false;
    }

    return compDni;
}