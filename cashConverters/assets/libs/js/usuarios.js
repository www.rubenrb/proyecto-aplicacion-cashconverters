
    var usuarios= new Array();

        $.ajax({
            type: "POST",
            url: "../php/tablaUsuarios.php",
            dataType: "json",
            
        })

        .done (function(data){

            usuarios = data;
            
            var cadenaUsuarios="";

            cadenaUsuarios+='<thead class="bg-light">';
                cadenaUsuarios+='<tr class="border-0">';
                    cadenaUsuarios+='<th class="border-0">Nombre</th>';
                    cadenaUsuarios+='<th class="border-0">Rol</th>';
                    cadenaUsuarios+='<th class="border-0">Estado</th>';
                cadenaUsuarios+='</tr>';
            cadenaUsuarios+='</thead>';
            cadenaUsuarios+='<tbody>';

            for(var i=0; i<usuarios.length; i++){

                var clase="";

                if(usuarios[i].estado=="Conectado"){

                    clase="badge-dot badge-success mr-1";
                }else{
                    clase="badge-dot badge-danger";
                }

                cadenaUsuarios+='<tr>';
                    cadenaUsuarios+='<td>'+usuarios[i].nombre+'</td>';
                    cadenaUsuarios+='<td>'+usuarios[i].rol+'</td>';
                    cadenaUsuarios+='<td><span class="'+clase+'"></span>'+usuarios[i].estado+'</td>';
                cadenaUsuarios+='</tr>';
            }
            cadenaUsuarios+='</tbody>';

            document.getElementById("UsuariosTabla").innerHTML=cadenaUsuarios;

            $(document).ready(function() {
                $('#UsuariosTabla').DataTable( {
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                } );
            } );

        })
        .fail (function() {
            console.log("no");
        });


        $(document).ready(function(){

            $('.logout').click(function(){
    
            $.ajax({
                    type: "POST", 
                    cache: false, 
                    url: "../php/logout.php", 
                    datatype: "html", 
                    data: $('.logout').serialize()
                    }) 
                    .done(function () {
    
                        window.location.href ="../php/login.php"   
                    })
    
                .fail(function () {
                            console.log("Fallo en ejecucion") 
                        });
            });
        });