var CompraVenta= new Array();
var clientes= new Array();
var vendedores= new Array();
var balances= new Array();
var categorias= new Array();
var compradores= new Array();
var categoria1;



    // ============================================================== 
    // Product Category
    // ============================================================== 

    $.ajax({
        type: "POST",
        url: "../php/categoriaVentas.php",
        dataType: "json",
        
    })

    .done (function(dataCat){
        categorias=dataCat;

        var chart = new Chartist.Pie('.ct-chart-category', {
            series: [categorias[0],categorias[1],categorias[2],categorias[3],categorias[4],categorias[5],categorias[6]],
            labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
        }, {
            donut: true,
            showLabel: false,
            donutWidth: 40
    
        });

        chart.on('draw', function(data) {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();
    
                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });
    
                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze'
                    }
                };
    
                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                }
    
                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px'
                });
    
                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });
    })
    .fail (function() {
        console.log("no");
    });


    var chart = new Chartist.Pie('.ct-chart-category', {
        series: [2,3,4,5],
        labels: ['Telefonía', 'Informatica', 'Videojuegos', 'Consolas', 'Electrodomesticos', 'TV/Video', 'Sonido']
    }, {
        donut: true,
        showLabel: false,
        donutWidth: 40

    });

    chart.on('draw', function(data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    


    // ============================================================== 
    // Customer acquisition
    // ============================================================== 

   

    chart.on('draw', function(data) {
        if (data.type === 'line' || data.type === 'area') {
            data.element.animate({
                d: {
                    begin: 2000 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
    });




    // ============================================================== 
    // Revenue Cards
    // ============================================================== 
    

    $.ajax({
        type: "POST",
        url: "../php/balanceVentas.php",
        dataType: "json",
        
    })

    .done (function(dataV){
        balancesV=dataV;

        $("#sparkline-revenue2").sparkline([balancesV[11],balancesV[10],balancesV[9],balancesV[8],balancesV[7],balancesV[6],balancesV[5],balancesV[4],balancesV[3],balancesV[2],balancesV[1],balancesV[0]], {
            type: 'line',
            width: '99.5%',
            height: '100',
            lineColor: '#5969ff',
            fillColor: '#dbdeff',
            lineWidth: 2,
            spotColor: undefined,
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined,
            resize: true
        });
        var Bventas_redondeado=balancesV[12].toFixed(2);
        document.getElementById("totalV").innerHTML=Bventas_redondeado+"€";

    })
    .fail (function() {
        console.log("no");
    });


    // ============================================================== 
    // Revenue By Categories
    // ============================================================== 

    $(document).ready(function(){

        $('.logout').click(function(){

        $.ajax({
                type: "POST", 
                cache: false, 
                url: "../php/logout.php", 
                datatype: "html", 
                data: $('.logout').serialize()
                }) 
                .done(function () {

                    window.location.href ="../php/login.php"   
                })

            .fail(function () {
                        console.log("Fallo en ejecucion") 
                    });
        });
    });


    $(document).ready(function(){

        $.ajax({
            type: "POST",
            url: "../php/ultimasventas.php",
            dataType: "json",
            
        })

        .done (function(data){

            CompraVenta = data;
            
            var cadenaArticuloss="";


            cadenaArticuloss+='<thead class="bg-light">';
                cadenaArticuloss+='<tr class="border-0">';
                    cadenaArticuloss+='<th class="border-0">#</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo de artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Artículo</th>';
                    cadenaArticuloss+='<th class="border-0">Estado</th>';
                    cadenaArticuloss+='<th class="border-0">Precio Base</th>';
                    cadenaArticuloss+='<th class="border-0">Incremento</th>';
                    cadenaArticuloss+='<th class="border-0">Iva</th>';
                    cadenaArticuloss+='<th class="border-0">Precio venta</th>';
                    cadenaArticuloss+='<th class="border-0">Nombre</th>';
                    cadenaArticuloss+='<th class="border-0">Dni</th>';
                    cadenaArticuloss+='<th class="border-0">Fecha</th>';
                    cadenaArticuloss+='<th class="border-0">Tipo</th>';
                cadenaArticuloss+='</tr>';
            cadenaArticuloss+='</thead>';
            cadenaArticuloss+='<tbody>';

            for(var i=0; i<CompraVenta.length; i++){

                var clase="";

                if(CompraVenta[i].tipo=="compra"){

                    clase="badge-dot badge-brand mr-1";
                }else{
                    clase="badge-dot badge-success mr-1";
                }

                cadenaArticuloss+='<tr>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].id+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].tipoArticulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].articulo+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].estado+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precio+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].incremento+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].iva+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].precioTotal+'€</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].nombre+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].dni+'</td>';
                    cadenaArticuloss+='<td>'+CompraVenta[i].fecha+'</td>';
                    cadenaArticuloss+='<td><span class="'+clase+'"></span>'+CompraVenta[i].tipo+'</td>';
                cadenaArticuloss+='</tr>';
            }

            cadenaArticuloss+='</tbody>';

            document.getElementById("tablaArticulos").innerHTML=cadenaArticuloss;

        })
        .fail (function() {
            console.log("no");
        });

    });

  
    // ============================================================== 
    // Numero cliente/compras/ventas
    // ============================================================== 
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "../php/recuento_ventas.php",
            dataType: "json",
            
        })

        .done (function(recuentoTotal){
            recuento=recuentoTotal;

            document.getElementById("totalClientes").innerHTML=recuento[0]+" clientes";
            document.getElementById("totalVentas").innerHTML=recuento[1] +" ventas";
            document.getElementById("totalProductos").innerHTML=recuento[2]+ " productos";

        })
        .fail (function() {
            console.log("no");
        });

    });


    $.ajax({
        type: "POST",
        url: "../php/tablaVendedores.php",
        dataType: "json",
        
    })

    .done (function(data){

        vendedores = data;
        var cadenaClientesV="";


        cadenaClientesV+='<thead class="bg-light">';
            cadenaClientesV+='<tr class="border-0">';
                cadenaClientesV+='<th class="border-0">Nombre</th>';
                cadenaClientesV+='<th class="border-0">Telefono</th>';
                cadenaClientesV+='<th class="border-0">Mail</th>';
                cadenaClientesV+='<th class="border-0">Direccion</th>';
                cadenaClientesV+='<th class="border-0">Dni</th>';
                cadenaClientesV+='<th class="border-0">Articulos comprados</th>';
                cadenaClientesV+='<th class="border-0">Cantidad gastada</th>';
            cadenaClientesV+='</tr>';
        cadenaClientesV+='</thead>';
        cadenaClientesV+='<tbody>';

        for(var i=0; i<vendedores.length; i++){

            cadenaClientesV+='<tr>';
                cadenaClientesV+='<td>'+vendedores[i].nom+'</td>';
                cadenaClientesV+='<td>'+vendedores[i].telf+'</td>';
                cadenaClientesV+='<td>'+vendedores[i].mail+'</td>';
                cadenaClientesV+='<td>'+vendedores[i].dir+'</td>';
                cadenaClientesV+='<td>'+vendedores[i].dni+'</td>';
                cadenaClientesV+='<td>'+vendedores[i].veces+' compras</td>';
                cadenaClientesV+='<td>'+vendedores[i].total+'€</td>';
            cadenaClientesV+='</tr>';
        }
        cadenaClientesV+='</tbody>';

        document.getElementById("tablaVendedores").innerHTML=cadenaClientesV;

        $(document).ready(function() {
            $('#tablaVendedores').DataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            } );
        } );

    })
    .fail (function() {
        console.log("no");
    });


    