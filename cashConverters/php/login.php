<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css'>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'>
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="../css/loginStyle.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
  <?php  


      if(isset($_GET['error'])){

        $error=$_GET['error'];
      }else{
        $error="";
      }
  ?>


<form id="loginform" action="comprobacion_login.php" method="post">
  <div class="container">
          <div class="row">
              <div class="col-lg-3 col-md-2"></div>
              <div class="col-lg-6 col-md-8 login-box">
                  <div class="col-lg-12 login-key">
                      <i class="fa fa-key" aria-hidden="true"></i>
                  </div>
                  <div class="col-lg-12 login-title">
                      Inicio de sesión
                  </div>


                  <div class="col-lg-12 login-form">
                      <div class="col-lg-12 login-form">
                          <form>
                              <div class="form-group">
                                  <label class="form-control-label">Nombre</label>
                                  <input type="text" name="user" class="form-control">
                              </div>
                              <div class="form-group">
                                  <label class="form-control-label">Contraseña</label>
                                  <input type="password" name="pass" class="form-control" autocomplete="on">
                              </div>

                              <div class="col-lg-12 loginbttm">
                                  <div class="col-lg-6 login-btm login-text">
                                      <!-- Error Message -->
                                  </div>
                                  <div class="col-lg-6 login-btm login-button">
                                      <button type="submit" name="ingresar" class="btn btn-outline-primary">LOGIN</button>
                                  </div> 
                                  <span id="rojo">

                                    <?php echo $error; ?>

                                  </span>                                    
                              </div>
                          </form>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-2"></div>
              </div>
          </div>
   </div>
</form>
<script src="../js/main.js"></script>
</body>
</html>
