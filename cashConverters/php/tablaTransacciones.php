
<?php
 header('Content-Type: application/json');
require("conexion.php");
$ar = array();
$arCompra = array();
$arVenta = array();

$sql1 = "SELECT id,TipoArticulo,Articulo,Estado,PrecioBase,PrecioCompra,NombreCompra,DniCompra,FechaCompra FROM compras";
$result1 = mysqli_query ($conn, $sql1);  

if ($result1 == TRUE) {
    while ($registro = mysqli_fetch_row($result1)) {
 
             $arCompra[] = array(
				'id' => $registro[0],
             	'tipoArticulo' => $registro[1],
             	'articulo' => $registro[2],
             	'estado' => $registro[3],
             	'precio' => $registro[4],
             	'precioTotal' => $registro[5],
				'nombre' => $registro[6],
             	'dni' => $registro[7],
				'tipo' => 'compra',
				'fecha' => $registro[8],

             );
                     
	}
}

$sql2 = "SELECT id,TipoArticulo,Articulo,Estado,PrecioBase,PrecioVenta,NombreVenta,DniVenta,FechaVenta FROM ventas";
$result2 = mysqli_query ($conn, $sql2);  

if ($result1 == TRUE) {
    while ($registro = mysqli_fetch_row($result2)) {
 
             $arVenta[] = array(
				'id' => $registro[0],
             	'tipoArticulo' => $registro[1],
             	'articulo' => $registro[2],
             	'estado' => $registro[3],
             	'precio' => $registro[4],
             	'precioTotal' => $registro[5],
				'nombre' => $registro[6],
             	'dni' => $registro[7],
				'tipo' => 'venta',
				'fecha' => $registro[8]
             );
                     
	}
}

if ($result1 == FALSE) {
var_dump("FALLO");
}

if ($result2 == FALSE) {
	var_dump("FALLO");
}

$ar = array_merge($arCompra, $arVenta);

echo json_encode($ar);
 
?>