
<?php
 header('Content-Type: application/json');
require("conexion.php");

$arCompra = array();

$sql1 = "SELECT id,TipoArticulo,Articulo,Estado,PrecioBase,PrecioCompra,NombreCompra,DniCompra,FechaCompra,IvaImpuesto FROM compras ORDER BY `id` DESC LIMIT 10";
$result1 = mysqli_query ($conn, $sql1);  

if ($result1 == TRUE) {
    while ($registro = mysqli_fetch_row($result1)) {
 
             $arCompra[] = array(
				'id' => $registro[0],
             	'tipoArticulo' => $registro[1],
             	'articulo' => $registro[2],
             	'estado' => $registro[3],
             	'precio' => $registro[4],
             	'precioTotal' => $registro[5],
				'nombre' => $registro[6],
             	'dni' => $registro[7],
				'fecha' => $registro[8],
				'iva' => $registro[9],
				'tipo' => 'compra'
             );
                     
	}
}


echo json_encode($arCompra);
 
?>