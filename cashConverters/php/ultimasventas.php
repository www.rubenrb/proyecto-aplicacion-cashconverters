
<?php
 header('Content-Type: application/json');
require("conexion.php");

$arCompra = array();

$sql1 = "SELECT id,TipoArticulo,Articulo,Estado,PrecioBase,PrecioVenta,NombreVenta,DniVenta,FechaVenta,Incremento,IvaVenta FROM ventas ORDER BY `id` DESC LIMIT 10";
$result1 = mysqli_query ($conn, $sql1);  

if ($result1 == TRUE) {
    while ($registro = mysqli_fetch_row($result1)) {
 
             $arCompra[] = array(
				'id' => $registro[0],
             	'tipoArticulo' => $registro[1],
             	'articulo' => $registro[2],
             	'estado' => $registro[3],
             	'precio' => $registro[4],
             	'precioTotal' => $registro[5],
				'nombre' => $registro[6],
             	'dni' => $registro[7],
				'fecha' => $registro[8],
				'incremento' => $registro[9],
				'iva' => $registro[10],
				'tipo' => 'venta'
             );
                     
	}
}


echo json_encode($arCompra);
 
?>