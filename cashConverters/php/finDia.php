
<?php

	require("conexion.php");

	$ventas=json_decode($_POST['data1']);
	$compras=json_decode($_POST['data2']);
	$almacen=json_decode($_POST['data3']);
	$balance=json_decode($_POST['data4']);
	$cliente=json_decode($_POST['data5']);

	$longitudCompra=count($compras);
	$longitudVenta=count($ventas);
	$longitudAdmacen=count($almacen);
	$longitudBalance=count($balance);
	$longitudClientes=count($cliente);

	$validoCompra=false;
	$validoVenta=false;
	$validoAlmacen=false;
	$validoBalance=false;
	$validoClientes=false;

	$f= date("Y").'-'.date("m").'-'.date("d");


	/* Modulo para subir todas las compras al servidor */

	for ($i = 0; $i < $longitudCompra; $i++) {

		$articuloC=($compras[$i]->{'articulo'});
		$dniC=($compras[$i]->{'dni'});
		$estadoC=($compras[$i]->{'estado'});
		$ivaC=($compras[$i]->{'iva'});
		$nombreC=($compras[$i]->{'nombre'});
		$precioC=($compras[$i]->{'precio'});
		$precioTotalC=($compras[$i]->{'precioTotal'});
		$tipoArticuloC=($compras[$i]->{'tipoArticulo'});

		$sql1= "INSERT INTO compras (id,TipoArticulo,Articulo,Estado,PrecioBase,IvaImpuesto,PrecioCompra,NombreCompra,DniCompra,FechaCompra) VALUES (NULL,'$tipoArticuloC','$articuloC','$estadoC','$precioC','$ivaC','$precioTotalC','$nombreC','$dniC','$f')";

		$result1 = mysqli_query ($conn, $sql1);

			if ($result1 == FALSE) {

				$validoCompra=false;
			}

			if ($result1 == TRUE) {

				$validoCompra=true;	             

			}

	}

	if($validoCompra==true){
	
		var_dump("La compra se ha enviado con exito al servidor.");

	}else{
		var_dump("Error en subida de las compras al servidor.");
	}

	/* Modulo para subir todas las ventas al servidor */

	for ($i = 0; $i < $longitudVenta; $i++) {

		$articuloV=($ventas[$i]->{'nombreArt'});
		$dniV=($ventas[$i]->{'dni'});
		$estadoV=($ventas[$i]->{'estado'});
		$nombreV=($ventas[$i]->{'nombre'});
		$precioVentaV=($ventas[$i]->{'precioVenta'});
		$tipoArticuloV=($ventas[$i]->{'tipoArt'});
		$precioBaseV=($ventas[$i]->{'precioBase'});
		$incrementoV=($ventas[$i]->{'incremento'});
		$ivaVentaV=($ventas[$i]->{'ivaVenta'});

		$sql2= "INSERT INTO ventas (id,TipoArticulo,Articulo,Estado,PrecioBase,Incremento,IvaVenta,precioVenta,NombreVenta,DniVenta,FechaVenta) VALUES (NULL,'$tipoArticuloV','$articuloV','$estadoV','$precioBaseV','$incrementoV','$ivaVentaV','$precioVentaV','$nombreV','$dniV','$f')";

		$result2 = mysqli_query ($conn, $sql2);

			if ($result2 == FALSE) {

				$validoVenta=false;
			}
			if ($result2 == TRUE) {

				$validoVenta=true;	             

			}

	}

	if($validoCompra==true){
	
		var_dump("La venta se ha enviado con exito al servidor.");

	}else{
		var_dump("Error en subida de las ventas al servidor.");
	}


	/* Modulo para subir los productos del almacen al servidor */

	for ($i = 0; $i < $longitudAdmacen; $i++) {

		$articuloA=($almacen[$i]->{'articulo'});
		$dniA=($almacen[$i]->{'dni'});
		$estadoA=($almacen[$i]->{'estado'});
		$ivaA=($almacen[$i]->{'iva'});
		$nombreA=($almacen[$i]->{'nombre'});
		$precioA=($almacen[$i]->{'precio'});
		$precioTotalA=($almacen[$i]->{'precioTotal'});
		$tipoArticuloA=($almacen[$i]->{'tipoArticulo'});

		$sql3= "INSERT INTO almacen (id,TipoArticulo,Articulo,Estado,PrecioBase,IvaImpuesto,PrecioCompra,NombreCompra,DniCompra,FechaCompra) VALUES (NULL,'$tipoArticuloA','$articuloA','$estadoA','$precioA','$ivaA','$precioTotalA','$nombreA','$dniA','$f')";

		$result3 = mysqli_query ($conn, $sql3);

			if ($result3 == FALSE) {

				$validoAlmacen=false;
			}

			if ($result3 == TRUE) {

				$sql5= "DELETE FROM almacen WHERE FechaCompra<>'$f'" ;
				$result5 = mysqli_query ($conn, $sql5);

				$validoAlmacen=true;	             

			}

	}

	if($validoAlmacen==true){
	
		var_dump("Se ha enviado los productos del almacen con exito al servidor.");

	}else{
		var_dump("Error en subida del almacen al servidor.");
	}

	/* Modulo para añadir los balances diarios al servidor */

		$gasto=($balance[0]);
		$ganancia=($balance[1]);
		$rec=($balance[2]);


		$sql4= "INSERT INTO balance (BalanceCompras,BalanceVentas,BalanceTotal,Fecha) VALUES ('$gasto','$ganancia','$rec','$f')";

		$result4 = mysqli_query ($conn, $sql4);

			if ($result4 == FALSE) {

				$validoBalance=false;
			}

			if ($result4 == TRUE) {

				$validoBalance=true;	             

			}

	if($validoBalance==true){
	
		var_dump("Se ha subido los balances con exito al servidor.");

	}else{
		var_dump("Error en subida del almacen al servidor.");
	}

	/* Modulo para añadir los clientes al servidor */

	for ($i = 0; $i < $longitudClientes; $i++) {

		$clienteNom=($cliente[$i]->{'nombre'});
		$clienteDni=($cliente[$i]->{'dni'});
		$clienteTlf=($cliente[$i]->{'telefono'});
		$clienteMail=($cliente[$i]->{'mail'});
		$clienteDir=($cliente[$i]->{'dir'});

		$sql6 = "SELECT Dni FROM clientes WHERE Dni = '$dniV'";
		$result6 = mysqli_query ($conn, $sql6);  

		if(mysqli_num_rows($result6) == 0){

			$sql7= "INSERT INTO clientes (Nombre,Dni,Telefono,Mail,Direccion) VALUES ('$clienteNom','$clienteDni','$clienteTlf','$clienteMail','$clienteDir')";
			$result5 = mysqli_query ($conn, $sql7);

		}

	}


?>
